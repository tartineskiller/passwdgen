#include <QApplication>
#include <iostream>
#include <string>
#include <QTextCodec>
#include <QTranslator>
#include <QTextCodec>
#include "password.h"
#include "mainw.h"
#include "utils.h"

using namespace std;

string Utils::stylesheetReady = "QPushButton { background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #00E800, stop: 0.4 #00DE00, stop: 0.5 #00D800, stop: 1.0 #00D300); border-style: outset; border-width: 2px; border-radius: 8px; border-color: gray; font: bold 14px; min-width: 10em; padding: 6px; } QPushButton:pressed { background-color: qlineargradient(x1:0, y1:1, x2:0, y2:0, stop:0 #00E800, stop: 0.4 #00DE00, stop: 0.5 #00D800, stop: 1.0 #00D300); border-style: inset; }";
string Utils::stylesheetBusy = "QPushButton { background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #E8E800, stop: 0.4 #DEDE00, stop: 0.5 #D8D800, stop: 1.0 #D3D300); border-style: outset; border-width: 2px; border-radius: 8px; border-color: gray; font: bold 14px; min-width: 10em; padding: 6px; }";

/** Si le programme est lancé avec des arguments, en premier le nombre de caractères, en troisième le type de mot de passe, l'interface graphique ne sera pas lancée.
 */

int main(int argc, char *argv[])
{
    srand(time(NULL)); //init du gén. nbe aléa

    if(argc > 2) //si mode console
    {
        Password p(atoi(argv[1]), atoi(argv[2]));
        p.createPasswd();
        cout << p.passwd() << endl;
    }
    else //si mode graphique
    {
        QApplication app(argc, argv);
        //force l'encodage en utf8 et tout et tout
//        QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
//        QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("iso-8859-1"));

        QLocale tmp;
        QString locale = tmp.uiLanguages().at(0).section('_', 0, 0);
        cout << locale.toStdString() << endl;

        QTranslator translator;
        translator.load("passwdgen_"+locale);
        app.installTranslator(&translator);

        Password *p = new Password;
        MainW mw(p);

        mw.show();
        return app.exec();
    }
}
