#include "typeslider.h"

TypeSlider::TypeSlider(): QWidget()
{
    //slider
    m_sli = new QSlider();
    m_sli->setRange(0, 3);
    m_sli->setPageStep(1);
    m_sli->setTickPosition(QSlider::NoTicks);
    m_sli->setTickInterval(1);
    m_sli->setOrientation(Qt::Horizontal);

    //labels
    m_lblChiffres = new QLabel("123");
    m_lblLettres = new QLabel("abc");
    m_lblUpper = new QLabel("  ABC");
    m_lblSymb = new QLabel("&\"@");

    //layouts
    m_gridLay = new QGridLayout();
    m_vlay = new QVBoxLayout(this);

    //positionnement
    //des labels
    m_gridLay->addWidget(m_lblChiffres, 0, 0);
    m_gridLay->addWidget(m_lblLettres, 0, 1);
    m_gridLay->addWidget(m_lblUpper, 0, 2);
    m_gridLay->addWidget(m_lblSymb, 0, 3);

    //coloration des labels
    m_lblChiffres->setStyleSheet("QLabel { color: black }");
    m_lblLettres->setStyleSheet("QLabel { color: gray }");
    m_lblUpper->setStyleSheet("QLabel { color: gray }");
    m_lblSymb->setStyleSheet("QLabel { color: gray }");

    //des autres composants
    m_vlay->addWidget(m_sli);
    m_vlay->addLayout(m_gridLay);

    this->setLayout(m_vlay);

    QObject::connect(m_sli, SIGNAL(valueChanged(int)), this, SLOT(couleurTexte()));
}

void TypeSlider::couleurTexte()
{
    //si ce slot est appellé, c'est qu'il faut mettre en avant un certain texte.
    int valSlider = m_sli->value();
    m_lblChiffres->setStyleSheet("QLabel { color: gray }");
    m_lblLettres->setStyleSheet("QLabel { color: gray }");
    m_lblUpper->setStyleSheet("QLabel { color: gray }");
    m_lblSymb->setStyleSheet("QLabel { color: gray }");

    switch(valSlider)
    {
    case 0:
        m_lblChiffres->setStyleSheet("QLabel { color: black }");
        break;
    case 1:
        m_lblLettres->setStyleSheet("QLabel { color: black }");
        break;
    case 2:
        m_lblUpper->setStyleSheet("QLabel { color: black }");
        break;
    default:
        m_lblSymb->setStyleSheet("QLabel { color: black }");
    }
}

int TypeSlider::value()
{
    return m_sli->value();
}

QSlider* TypeSlider::slider()
{
    return m_sli;
}
