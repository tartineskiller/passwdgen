#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QPushButton>
#include <QTextEdit>
#include <QLabel>
#include <QFormLayout>
#include <QSpinBox>
#include <QLineEdit>
#include <QKeyEvent>
#include <QFileDialog>
#include <QGroupBox>
#include "typeslider.h"
#include "password.h"
#include "sliderforcemdp.h"

#define TEXT_BOU_READY "GO !"
#define TEXT_BOU_BUSY "..."

class WidgetPrincipal: public QWidget //pas besoin de faire une MainWindow pour ça...
{
    Q_OBJECT

public:
    WidgetPrincipal(Password *p);
    ~WidgetPrincipal();
    void faireConnexions();
    void keyPressEvent(QKeyEvent *event);

protected:
    //layouts
    QVBoxLayout *m_hboxOpt;
    QVBoxLayout *m_hboxRes;
    QHBoxLayout *m_mainLay;
    QFormLayout *m_formSpin;
    QFormLayout *m_formProg;
    QGroupBox *m_groupConfig;
    QGroupBox *m_groupRes;

    //lineedit
    QLineEdit *m_lineSize;

    //spinbox
    QSpinBox *m_spin;

    //typeslider
    TypeSlider *m_ts;

    //pushbutton
    QPushButton *m_bouGo;
    QPushButton *m_bouCopy;

    //textedit
    QTextEdit *m_textPasswd;

    //titre
    QLabel *m_lblTitre;

    Password *m_p;
    ProgressForceMdp *m_sliMdp;

protected slots:
    void changeType();
    void changeTaille();
    void generePass();
    void debGenBou();
    void finGenBou();
    void enregistrerMdp();
    void copyMdp();

signals:
    void debutGeneration();
    void finGeneration();
};

#endif // FENETREPRINCIPALE_H
