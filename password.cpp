#include "password.h"

Password::Password(int size, int type)
{
    m_passwd = "";
    m_size = size;
    m_type = type;
}

void Password::setType(const Password::Type type)
{
    m_type = type;
}

void Password::setType(int type)
{
    m_type = type;
}

void Password::setSize(const unsigned int size)
{
    m_size = size;
}

std::string Password::passwd()
{
    return m_passwd;
}

bool Password::createPasswd()
{
    if(m_passwd != "") //si déjà un mdp généré
        m_passwd.clear();

    if(m_size > 0 && m_type != Password::Numbers) //pour juste un chiffre, voir un peu plus bas
    {
        int tmp; //pour la selection du type de caractère
        for(int i = 0 ; i < m_size ; i++)
        {
            tmp = Utils::randomInt(m_type);
            switch(tmp)
            {
            case 0: //chiffre
                m_passwd += Utils::getInt();
                break;
            case 1: //lettre min
                m_passwd += Utils::getLetter();
                break;
            case 2: //upper letter
                m_passwd += Utils::getUpperLetter();
                break;
            default:
                m_passwd += Utils::getSymbol();
            }
        }
        return true;
    }
    else if(m_size > 0)
    {
        for(int i = 0 ; i < m_size ; i++)
        {
            m_passwd += Utils::getInt();
        }
        return true;
    }
    else
        return false;
}

void Password::savePasswd(std::string path) //enregistrer le mdp sous...
{
    std::ofstream flux(path.c_str());

    if(!flux)
        exit(2);
    flux << m_passwd << std::endl;

    flux.close();
}

int Password::size()
{
    return m_size;
}

int Password::nbeLettreLangage()
{
    switch(m_type)
    {
    case Numbers:
        return 9;
    case Letters:
        return 35;
    case UpperLetters:
        return 61;
    default: //Symbols
        return 94;
    }
}

int Password::type()
{
    return m_type;
}

