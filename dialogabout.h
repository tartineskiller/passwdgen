#ifndef DIALOGABOUT_H
#define DIALOGABOUT_H

#include <QDialog>
#include <QIcon>
#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include "constantes.h"
#define VERSION "1.0.2"

class DialogAbout: public QDialog
{
    Q_OBJECT

public:
    DialogAbout(QWidget *parent);
    void faireConnexions();

protected:
    QVBoxLayout *m_vbox;
    QLabel *m_lblLogo;
    QLabel *m_lblNom;
    QLabel *m_lblVer;
    QLabel *m_lblDescr;
    QLabel *m_lblvide;
    QLabel *m_lblAuteur;
    QLabel *m_lblGit;
    QPushButton *m_bouFerm;
};

#endif // DIALOGABOUT_H
