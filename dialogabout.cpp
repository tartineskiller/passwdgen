#include "dialogabout.h"

DialogAbout::DialogAbout(QWidget *parent): QDialog(parent)
{
    m_vbox = new QVBoxLayout(this);
    m_lblLogo = new QLabel;

    m_lblLogo->setAlignment(Qt::AlignHCenter);
    m_lblNom = new QLabel(tr("Password generator"));
    m_lblNom->setAlignment(Qt::AlignHCenter);
    m_lblNom->setFont(QFont("Ubuntu", 20));
    m_lblVer = new QLabel(VERSION);
    m_lblVer->setAlignment(Qt::AlignHCenter);
    m_lblDescr = new QLabel(tr("A simple password generator."));
    m_lblDescr->setAlignment(Qt::AlignHCenter);
    m_lblvide = new QLabel;
    m_lblAuteur = new QLabel("Damien Pijaczynski <a href=\"mailto:tartineskiller@gmail.com\">tartineskiller@gmail.com</a>");
    m_lblAuteur->setOpenExternalLinks(true); //autoriser l'envoi d'email depuis le lbl
    m_lblAuteur->setFont(QFont("Ubuntu", 8));
    m_lblAuteur->setAlignment(Qt::AlignHCenter);
    m_lblGit = new QLabel(tr("Source code available <a href='https://bitbucket.org/tartineskiller/passwdgen'>here</a>"));
    m_lblGit->setOpenExternalLinks(true);
    m_lblGit->setFont(QFont("Ubuntu", 8));
    m_lblGit->setAlignment(Qt::AlignCenter);
    m_bouFerm = new QPushButton(tr("Close"));

    m_vbox->addWidget(m_lblLogo);
    m_vbox->addWidget(m_lblNom);
    m_vbox->addWidget(m_lblVer);
    m_vbox->addWidget(m_lblDescr);
    m_vbox->addWidget(m_lblvide);
    m_vbox->addWidget(m_lblAuteur);
    m_vbox->addWidget(m_lblGit);
    m_vbox->addWidget(m_bouFerm);

    faireConnexions();

    //setFixedSize(310, 310);

    setLayout(m_vbox);
}

void DialogAbout::faireConnexions()
{
    QObject::connect(m_bouFerm, SIGNAL(clicked()), this, SLOT(close()));
}
