#ifndef PASSWORD_H
#define PASSWORD_H

/** Représente un mot de passe.
 **/

#include <string>
#include <fstream>
#include "utils.h"

class Password
{
public:
    enum Type
    {
        Numbers = 0,
        Letters = 1,
        UpperLetters = 2,
        Symbols = 3
    };

    Password(int size=10, int type=Password::Numbers);
    void setType(Password::Type const type);
    void setType(int type);
    void setSize(unsigned int const size);
    std::string passwd();
    bool createPasswd();
    void savePasswd(std::string path);
    int size();
    int nbeLettreLangage(); //nbe de caractères dans le langage utilisé pour le mdp
    int type();

protected:
    std::string m_passwd;
    int m_size;
    int m_type; //enum juste au dessus
};

#endif // PASSWORD_H
