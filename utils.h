#ifndef UTILS_H
#define UTILS_H

#include <time.h>
#include <stdlib.h>
#include <string>

class Utils
{
public:
    Utils();
    static int randomInt(int min, int max);
    static int randomInt(int max);
    static char getInt();
    static char getLetter();
    static char getUpperLetter();
    static char getSymbol();
    static std::string stylesheetReady;
    static std::string stylesheetBusy;
};

#endif // UTILS_H
