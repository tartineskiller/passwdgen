#include "mainw.h"

MainW::MainW(Password *p)
{
    m_wp = new WidgetPrincipal(p);
    setCentralWidget(m_wp);

    setWindowTitle(tr("Password generator"));

#ifdef BUILD_NOINSTALL
    setWindowIcon(QIcon("./icon.svg"));
#endif

#ifdef BUILD_LINUX
    setWindowIcon(QIcon("/opt/passwdgen/icon.svg"));
#endif

    //les menus
    m_menuFichier = menuBar()->addMenu(tr("&File"));
    m_menuAide = menuBar()->addMenu("&?");

    //elts de menu
    m_actEnreg = new QAction(tr("&Save as..."), this);
    m_actQuit = new QAction(tr("&Exit"), this);
    m_actAbout = new QAction(tr("About..."), this);
    m_actAboutQt = new QAction(tr("About Qt"), this);

    m_menuFichier->addAction(m_actEnreg);
    m_menuFichier->addAction(m_actQuit);
    m_menuAide->addAction(m_actAbout);
    m_menuAide->addAction(m_actAboutQt);

    setFixedSize(LARG, HAUT);

    faireConnexions();
}

MainW::~MainW()
{
    delete m_wp;
}

void MainW::faireConnexions()
{
    QObject::connect(m_actEnreg, SIGNAL(triggered()), m_wp, SLOT(enregistrerMdp()));
    QObject::connect(m_actQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
    QObject::connect(m_actAbout, SIGNAL(triggered()), this, SLOT(about()));
    QObject::connect(m_actAboutQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
}

void MainW::about()
{
    DialogAbout a(this);
    a.exec();
}
