#include "utils.h"

Utils::Utils()
{
}

char Utils::getInt()
{
    return Utils::randomInt(8)+48;
}

char Utils::getLetter()
{
    return Utils::randomInt(25)+97;
}

char Utils::getUpperLetter()
{
    return Utils::randomInt(25)+65;
}

char Utils::getSymbol()
{
    //puisque les symboles sont un peu épars vis-à-vis du code ascii, on va déjà selectionner un nbe compris entre 0 et 3 pour "choisir" quel morceau on veut
    int tmp;
    tmp = Utils::randomInt(3);
    switch(tmp)
    {
    case 0:
        return Utils::randomInt(15)+32;
    case 1:
        return Utils::randomInt(6)+58;
    case 2:
        return Utils::randomInt(5)+91;
    default:
        return Utils::randomInt(3)+123;
    }
}

int Utils::randomInt(int min, int max)
{
    return (min + (rand () % (max - min+1)));
}

int Utils::randomInt(int max)
{
    return Utils::randomInt(0, max); //on considère que min est 0
}
