#include "widgetprincipal.h"

WidgetPrincipal::WidgetPrincipal(Password *p): QWidget(0)
{
    m_p = p;

    setWindowTitle(tr("Password generator"));

    m_formSpin = new QFormLayout;
    m_formProg = new QFormLayout;

    //spinbox
    m_spin = new QSpinBox;
    m_spin->setMinimum(1);
    m_spin->setMaximum(1000000);
    m_spin->setValue(10);
    m_spin->setSingleStep(5);

    //slider
    m_ts = new TypeSlider;

    //pushbutton
    m_bouGo = new QPushButton(TEXT_BOU_READY);
    m_bouCopy = new QPushButton(tr("Copy to clipboard"));

    //textedit
    m_textPasswd = new QTextEdit;
    m_textPasswd->setReadOnly(true); //l'utilisateur n'a pas le droit de modifier le mdp
    m_textPasswd->setFixedHeight(50);
    m_textPasswd->setFont(QFont("Courier"));
    m_textPasswd->setWordWrapMode(QTextOption::WrapAnywhere);

    m_formSpin->addRow(tr("Size"), m_spin);

    m_groupConfig = new QGroupBox(tr("Options"));
    m_groupRes = new QGroupBox(tr("Result"));

    m_hboxOpt = new QVBoxLayout;
    m_hboxOpt->addLayout(m_formSpin);
    m_hboxOpt->addWidget(m_ts);
    m_hboxOpt->addWidget(m_bouGo);

    m_groupConfig->setLayout(m_hboxOpt);

    m_sliMdp = new ProgressForceMdp(m_p);
    m_sliMdp->setToolTip(tr("This is only a estimation, couldn't be trusted at 100% !"));

    m_formProg->addRow(tr("Strength"), m_sliMdp);

    m_hboxRes = new QVBoxLayout;
    m_hboxRes->addLayout(m_formProg);
    m_hboxRes->addWidget(m_bouCopy);
    m_hboxRes->addWidget(m_textPasswd);

    m_groupRes->setLayout(m_hboxRes);

    m_mainLay = new QHBoxLayout;
    m_mainLay->addWidget(m_groupConfig);
    m_mainLay->addWidget(m_groupRes);

    m_bouGo->setStyleSheet(Utils::stylesheetReady.c_str());

    setLayout(m_mainLay);

    faireConnexions();
}

WidgetPrincipal::~WidgetPrincipal()
{
    delete m_p;
}

void WidgetPrincipal::faireConnexions()
{
    QObject::connect(m_spin, SIGNAL(editingFinished()), this, SLOT(changeTaille()));
    QObject::connect(m_ts->slider(), SIGNAL(valueChanged(int)), this, SLOT(changeType()));
    QObject::connect(m_bouGo, SIGNAL(clicked()), this, SLOT(generePass()));
    QObject::connect(m_bouGo, SIGNAL(clicked()), m_sliMdp, SLOT(actualise()));
    QObject::connect(this, SIGNAL(debutGeneration()), this, SLOT(debGenBou()));
    QObject::connect(this, SIGNAL(finGeneration()), this, SLOT(finGenBou()));
    QObject::connect(m_bouCopy, SIGNAL(clicked()), this, SLOT(copyMdp()));
}

void WidgetPrincipal::keyPressEvent(QKeyEvent *event)
{
    int key = event->key();

    switch (key)
    {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        generePass();
        m_sliMdp->actualise(); //on met à jour la force
        break;
    default:
        QWidget::keyPressEvent(event);
    }
}

void WidgetPrincipal::changeType()
{
    m_p->setType(m_ts->value());
}

void WidgetPrincipal::changeTaille()
{
    m_p->setSize(m_spin->value());
}

void WidgetPrincipal::generePass()
{
    emit debutGeneration();
    this->repaint();
    m_p->createPasswd();
    m_textPasswd->setText(m_p->passwd().c_str());
    emit finGeneration();
}

void WidgetPrincipal::debGenBou()
{
    m_bouGo->setStyleSheet(Utils::stylesheetBusy.c_str());
    m_bouGo->setText(TEXT_BOU_BUSY);
}

void WidgetPrincipal::finGenBou()
{
    m_bouGo->setStyleSheet(Utils::stylesheetReady.c_str());
    m_bouGo->setText(TEXT_BOU_READY);
}

void WidgetPrincipal::enregistrerMdp()
{
    QString chem = QFileDialog::getSaveFileName(this, tr("Save as..."), "passwd.txt", ".txt (*.txt)");
    if(chem != "")
        m_p->savePasswd(chem.toStdString());
}

void WidgetPrincipal::copyMdp()
{
    m_textPasswd->selectAll(); //obligé de tout slc pour copier
    m_textPasswd->copy();
}

/* Ancien desing de la version 1.0.0 */
//WidgetPrincipal::WidgetPrincipal(Password *p): QWidget()
//{
//    m_p = p;

//    setWindowTitle(tr("Générateur de mots de passe"));
//    setWindowIcon(QIcon("./icon.jpg"));

//    //les layouts
//    m_grid2 = new QGridLayout;
//    m_vbox = new QVBoxLayout(this);
//    m_grid1 = new QGridLayout;

//    //le label titre
//    m_lblTitre = new QLabel("Password Generator");
//    m_lblTitre->setFont(QFont("Ubuntu", 25)); //à changer

//    //form
//    m_form = new QFormLayout;

//    //spinbox
//    m_spin = new QSpinBox;
//    m_spin->setMinimum(1);
//    m_spin->setMaximum(1000000);
//    m_spin->setValue(10);
//    m_spin->setSingleStep(5);

//    //typeslider
//    m_ts = new TypeSlider;

//    //pushbutton
//    m_bouGo = new QPushButton(TEXT_BOU_READY);

//    //textedit
//    m_textPasswd = new QTextEdit;
//    m_textPasswd->setReadOnly(true); //l'utilisateur n'a pas le droit de modifier le mdp
//    m_textPasswd->setFixedHeight(50);
//    m_textPasswd->setFont(QFont("Courier"));
//    m_textPasswd->setWordWrapMode(QTextOption::WrapAnywhere);

//    //placement, etc...
//    m_form->addRow("Size", m_spin);
//    m_grid2->addLayout(m_form, 0, 0);
//    m_grid2->addWidget(m_ts, 0, 1);
//    m_grid2->setColumnMinimumWidth(0, this->width()*(0.3));

//    m_grid1->addWidget(m_bouGo, 0, 0);
//    m_grid1->addWidget(m_textPasswd, 0, 1, 2, 1); //2 rowspan

//    m_vbox->addWidget(m_lblTitre);
//    m_vbox->addLayout(m_grid2);
//    m_vbox->addLayout(m_grid1);

//    faireConnexions();
//    m_bouGo->setStyleSheet(Utils::stylesheetReady.c_str());

//    this->setLayout(m_vbox);
//}
