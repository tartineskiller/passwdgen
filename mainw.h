#ifndef MAINW_H
#define MAINW_H

#include <QApplication>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QAction>

#include "password.h"
#include "widgetprincipal.h"
#include "dialogabout.h"
#include "constantes.h"

#define LARG 470
#define HAUT 190

class MainW: public QMainWindow
{
    Q_OBJECT

public:
    MainW(Password *p);
    ~MainW();
    void faireConnexions();

protected:
    WidgetPrincipal *m_wp;
    QMenu *m_menuFichier;
    QMenu *m_menuAide;
    QAction *m_actEnreg;
    QAction *m_actQuit;
    QAction *m_actAbout;
    QAction *m_actAboutQt;

protected slots:
    void about();
};

#endif // MAINW_H
