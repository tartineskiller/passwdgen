#include "sliderforcemdp.h"

ProgressForceMdp::ProgressForceMdp(Password *passwd): QProgressBar(0)
{
    m_mdp = passwd;

    setRange(0, 1000); //si sup à 1000, on ignorera
    setValue(0);
}

int ProgressForceMdp::calculeForce()
{
    if(m_mdp->size() < 6)
        return 25;
    int res;
    switch(m_mdp->type())
    {
    case Password::Numbers:
        res = 50;
        break;
    case Password::Letters:
        res = 200;
        break;
    case Password::UpperLetters:
        res = 350;
        break;
    default:
        res = 400;
    }

    int longTmp = m_mdp->size();

    while(longTmp > 10)
    {
        longTmp -= 10;
        res += 400;
    }

    if(res <= 1000)
        return res;

    else
        return 1000;
}

void ProgressForceMdp::actualise()
{
    setRange(0, 1000);
    setValue(calculeForce());
}
