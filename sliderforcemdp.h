#ifndef SLIDERFORCEMDP_H
#define SLIDERFORCEMDP_H

#include <iostream>
#include <QProgressBar>
#include <QString>
#include <cmath>
#include "password.h"

class ProgressForceMdp: public QProgressBar
{
    Q_OBJECT
public:
    ProgressForceMdp(Password *passwd);
    int calculeForce();

public slots:
    void actualise();

protected:
    Password *m_mdp;
};

#endif // SLIDERFORCEMDP_H
