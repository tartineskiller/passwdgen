<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="dialogabout.cpp" line="9"/>
        <source>Password generator</source>
        <translation>Générateur de mot de passe</translation>
    </message>
    <message>
        <location filename="dialogabout.cpp" line="14"/>
        <source>A simple password generator.</source>
        <translation>Un simple générateur de mots de passe.</translation>
    </message>
    <message>
        <location filename="dialogabout.cpp" line="21"/>
        <source>Source code available &lt;a href=&apos;https://bitbucket.org/tartineskiller/passwdgen&apos;&gt;here&lt;/a&gt;</source>
        <translation>Code source disponible &lt;a href=&apos;https://bitbucket.org/tartineskiller/passwdgen&apos;&gt;ici&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="dialogabout.cpp" line="25"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>MainW</name>
    <message>
        <location filename="mainw.cpp" line="8"/>
        <source>Password generator</source>
        <translation>Générateur de mot de passe</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="19"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="23"/>
        <source>&amp;Save as...</source>
        <translation>&amp;Sauvegarder sous...</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="24"/>
        <source>&amp;Exit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="25"/>
        <source>About...</source>
        <translation>À propos...</translation>
    </message>
    <message>
        <location filename="mainw.cpp" line="26"/>
        <source>About Qt</source>
        <translation>À propos de Qt</translation>
    </message>
</context>
<context>
    <name>WidgetPrincipal</name>
    <message>
        <location filename="widgetprincipal.cpp" line="7"/>
        <source>Password generator</source>
        <translation>Générateur de mot de passe</translation>
    </message>
    <message>
        <location filename="widgetprincipal.cpp" line="24"/>
        <source>Copy to clipboard</source>
        <translation>Copier dans le presse-papier</translation>
    </message>
    <message>
        <location filename="widgetprincipal.cpp" line="33"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="widgetprincipal.cpp" line="35"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="widgetprincipal.cpp" line="36"/>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <location filename="widgetprincipal.cpp" line="46"/>
        <source>This is only a estimation, couldn&apos;t be trusted at 100% !</source>
        <translation>Ceci n&apos;est qu&apos;une estimation, pas de garanties !</translation>
    </message>
    <message>
        <location filename="widgetprincipal.cpp" line="48"/>
        <source>Strength</source>
        <translation>Force</translation>
    </message>
    <message>
        <location filename="widgetprincipal.cpp" line="133"/>
        <source>Save as...</source>
        <translation>Sauvegarder sous...</translation>
    </message>
</context>
</TS>
