#ifndef TYPESLIDER_H
#define TYPESLIDER_H

#include <QSlider>
#include <QWidget>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QLabel>

/** Slider pour définir le type du mdp génété
 */

class TypeSlider: public QWidget
{
    Q_OBJECT
public:
    TypeSlider();
    int value();
    QSlider *slider();

protected:
    QGridLayout *m_gridLay;
    QVBoxLayout *m_vlay;

    QLabel *m_lblChiffres;
    QLabel *m_lblLettres;
    QLabel *m_lblUpper;
    QLabel *m_lblSymb;

    QSlider *m_sli;

protected slots:
    void couleurTexte();
};

#endif // TYPESLIDER_H
